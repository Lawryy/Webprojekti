# README

* Android-sovellukseen liittyvät tiedostot löytyvät kansiosta `BaarikaappiApp`
* NodeJS palvelimeen liittyvät tiedostot löytyvät kansiosta `node`, lukuunottamatta tietokannan inserttejä
    - Kannan insertit löytyvät `drive`-kansion tiedostosta reseptejä.json
    - main.js -tiedosto oleellisin
* Eri .ejs-tiedostot löytyvät kansiosta node/views
* Kansiosta `drive` löytyy myös demovideo sovelluksesta
* Tietojen kantaan laittamiseen käytettiin tiedostoa `node/mongoTest.js`
* Esittelyvideo.ogv:ssä esitellään webbisivun toimintaa