var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');

var url = 'mongodb://localhost:27017/BAARI_TESTI';
MongoClient.connect(url, function(err, db) {
  if(err) { return console.dir(err); }
  var collection = db.collection('res_2');
  var lotsOfDocs = [
      {
      	"name" : "Rommikola",
      	"ingredients" : [
      		{
      			"ingredient" : "Rommi",
      			"amount" : "4 cl"
      		},
      		{
      			"ingredient" : "Kola",
      			"amount" : "1 dl"
      		}
      	],
      	"description" : "Sekoita kaikki ainekset keskenään :)"
      },
      {
          "name" : "Mojito",
          "ingredients" : [
              {
                  "ingredient" : "Vaalea rommi",
                  "amount" : "4 cl"
              },
              {
                  "ingredient" : "Limemehu",
                  "amount" : "2 cl"
              },
              {
                  "ingredient" : "Mintun oksa",
                  "amount" : "2 kpl"
              },
              {
                  "ingredient" : "Sokerisiirappi",
                  "amount" : "1.5 cl"
              },
              {
                  "ingredient" : "Soodavesi",
                  "amount" : "6 cl"
              }
          ],
          "description" : "Murskaa minttu siirapin kanssa tarjoilulasin pohjalla. Lisää rommi ja limemehu. Täytä jäämurskalla. Lisää soodavesi. Koristele limelohkolla ja mintunlehdillä."
      },
      {
          "name": "El Capitan",
          "ingredients" : [
              {
                  "ingredient" : "Pisco",
                  "amount" : "4 cl"
              },
              {
                  "ingredient" : "Makea vermutti",
                  "amount" : "3 cl"
              },
              {
                  "ingredient" : "Angostura katkero",
                  "amount" : "0.1 cl"
              }
          ],
          "description" : "Sekoita sekoituslasissa jäiden kera. Suodata viilennettyyn lasiin. Koristele sitruunankuorella."
      },
      {
          "name" : "Caipirissima",
          "ingredients" : [
              {
                  "ingredient" : "Vaalea rommi",
                  "amount" : "4 cl"
              },
              {
                  "ingredient" : "Lime lohkoina",
                  "amount" : "0.5 kpl"
              },
              {
                  "ingredient" : "Sokerisiirappi",
                  "amount" : "1.5 cl"
              }
          ],
          "description" : "Murskaa limelohkot siirapin kanssa ravistimen pohjalla. Lisää rommi. Ravista murskatun jään kera. Kumoa lasiin."
      }
  ];

  collection.insert(lotsOfDocs, {w:1}, function(err, result) {});

  db.close();
});
