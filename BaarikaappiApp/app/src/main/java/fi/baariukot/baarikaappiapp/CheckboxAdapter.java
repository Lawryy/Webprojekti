package fi.baariukot.baarikaappiapp;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by karmaleivos on 24/10/2017.
 */

public class CheckboxAdapter extends ArrayAdapter<String>  {
    private Context context;
    //private LayoutInflater mInflater;
    private int mViewResourceId;
    private String[] mStrings;
    private ArrayList<String> items;

    private ArrayList<String> selectedStrings = new ArrayList<String>();

    public CheckboxAdapter(Context context,ArrayList<String> items){
        super(context,R.layout.list_item,R.id.ingredient,items);

        this.context = context;

        this.items = items;
    }

    /*
    public int getCount(){
        return mStrings.length;
    }

    public String getItem(int position){
        return mStrings[position];
    }

    public long getItemId(int position){
        return 0;
    }
    */

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        //convertView = mInflater.inflate(mViewResourceId, null);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.list_item, parent, false);

        CheckBox checkBox = itemView.findViewById(R.id.ingredient);
        checkBox.setText(items.get(position));

        /*
        tv.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    selectedStrings.add(tv.getText().toString());
                }
                else {
                    selectedStrings.remove(tv.getText().toString());
                }
            }
        });
        */
        return itemView;
    }

    public ArrayList<String> getSelectedString(){
        return selectedStrings;
    }
}
