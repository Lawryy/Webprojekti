package fi.baariukot.baarikaappiapp;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ChosenRecipe extends AppCompatActivity {

    private String TAG = MainActivity.class.getSimpleName();
    private ProgressBar progressBar;

    // URL to get recipe JSON
    private String baseUrl;
    private String recipeUrl;

    private JSONObject drinkJson;

    private String receivedDrink;

    private String drinkName;
    private ArrayList<String> ingredients;
    private ArrayList<String> amounts;
    private String drinkDescription;

    private ListAdapter listAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chosen_recipe);

        baseUrl = getString(R.string.baseURL);
        recipeUrl = "http://" + baseUrl + ":3000/drinks/search?d=";

        Bundle extras = getIntent().getExtras();
        receivedDrink = extras.getString("drink");
        Log.d("Received...", receivedDrink);

        ingredients = new ArrayList<String>();
        amounts = new ArrayList<String>();

        new GetDrink().execute();
    }

    /**
     * Async task class to get json by making HTTP call
     */
    private class GetDrink extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar = new ProgressBar(getApplicationContext(), null, android.R.attr.progressBarStyleSmall);
            progressBar.setVisibility(android.view.View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... args) {
            HttpHandler httpHandler = new HttpHandler();
            String fullUrl = recipeUrl + receivedDrink;
            Log.d("Full URL", fullUrl);

            String jsonString = httpHandler.makeServiceCall(fullUrl);

            Log.d(TAG, "Response from url: " + jsonString);

            if (jsonString != null) {
                try {
                    JSONArray returnedDrinks = new JSONArray(jsonString);
                    drinkJson = returnedDrinks.getJSONObject(0);

                    drinkName = drinkJson.getString("name");

                    int count = drinkJson.getJSONArray("ingredients").length();
                    Log.d("Count", ""+count);
                    for (int i = 0; i < count; i++) {
                        String name = drinkJson.getJSONArray("ingredients").getJSONObject(i).getString("ingredient");
                        String amount = drinkJson.getJSONArray("ingredients").getJSONObject(i).getString("amount");

                        Log.d("Added...", name + ";" + amount);

                        ingredients.add(name);
                        amounts.add(amount);
                    }

                    drinkDescription = drinkJson.getString("description");
                }
                catch (final JSONException e) {
                    Log.e(TAG, "Json parsing error: " + e.getMessage());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),
                                    "Json parsing error: " + e.getMessage(),
                                    Toast.LENGTH_LONG)
                                    .show();
                        }
                    });
                }
            }

            else {
                Log.e(TAG, "Couldn't get json from server.");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(),
                                "Couldn't get json from server. Check LogCat for possible errors!",
                                Toast.LENGTH_LONG)
                                .show();
                    }
                });

            }

            return null;
        }

        /**
         * Updating parsed JSON data into ListView
         * */
        @Override
        protected void onPostExecute(Void result) {
            // Dismiss the progress dialog
            progressBar.setVisibility(android.view.View.GONE);
            //lv.setAdapter((ListAdapter) new CheckboxAdapter(getApplicationContext(),R.layout))

            super.onPostExecute(result);

            TextView title = (TextView) findViewById(R.id.drink_title);
            title.setText(drinkName);

            ListView listView = (ListView) findViewById(R.id.ingredients_list);

            // https://stackoverflow.com/a/18530167
            listAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.recipe_item, R.id.ingredient_name, ingredients) {
                @Override
                public View getView(int position, View convertView, ViewGroup parent) {
                    View view = super.getView(position,convertView,parent);
                    TextView text1 = (TextView) view.findViewById(R.id.ingredient_name);
                    TextView text2 = (TextView) view.findViewById(R.id.ingredient_amount);

                    text1.setText(ingredients.get(position));
                    text2.setText(amounts.get(position));
                    Log.d("Set...", ingredients.get(position)+";"+amounts.get(position));
                    return view;
                }
            };
            listView.setAdapter(listAdapter);

            TextView description = (TextView) findViewById(R.id.drink_description);
            description.setText(drinkDescription);

            //adapter = new CheckboxAdapter(getApplicationContext(),ingredientList);
            /*
            listAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.list_item, R.id.name,drinks);
            lv.setAdapter(listAdapter);
            */
        }
    }
}
