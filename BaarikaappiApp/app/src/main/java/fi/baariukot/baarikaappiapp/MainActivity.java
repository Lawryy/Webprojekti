package fi.baariukot.baarikaappiapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.Toast;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Collections;

public class MainActivity extends AppCompatActivity {

    private String TAG = MainActivity.class.getSimpleName();

    private ProgressBar progressBar;
    ListView lv;

    // URL to get ingredients JSON
    private String baseUrl;
    private String ingredientUrl;

    private ArrayList<HashMap<String, String>> contactList;
    private ArrayList<String> ingredientList;

    private ArrayList<String> selected;
    private ArrayList<String> selected_new;

    private CheckboxAdapter adapter;
    private ListAdapter listAdapter;

    SparseBooleanArray sparseBooleanArray;
    SparseBooleanArray sparseBooleanArray_new;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        baseUrl = getString(R.string.baseURL);
        ingredientUrl = "http://" + baseUrl + ":3000/ingredients";

        lv = (ListView)findViewById(R.id.list);
        contactList = new ArrayList<>();

        ingredientList = new ArrayList<>();

        new GetIngredients().execute();
    }


    /**
     * Async task class to get json by making HTTP call
     */
    private class GetIngredients extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar = new ProgressBar(getApplicationContext(), null, android.R.attr.progressBarStyleSmall);
            progressBar.setVisibility(android.view.View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... args) {
            HttpHandler httpHandler = new HttpHandler();

            Log.d("Full URL", ingredientUrl);
            String jsonString = httpHandler.makeServiceCall(ingredientUrl);
            Log.d(TAG, "Response from url: " + jsonString);

            if (jsonString != null) {
                try {
                    JSONArray ingredients = new JSONArray(jsonString);
                    for (int i = 0; i < ingredients.length(); i++) {
                        ingredientList.add(ingredients.getString(i));
                    }
                }
                catch (final JSONException e) {
                    Log.e(TAG, "Json parsing error: " + e.getMessage());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),
                                    "Json parsing error: " + e.getMessage(),
                                    Toast.LENGTH_LONG)
                                    .show();
                        }
                    });
                }
            }

            else {
                Log.e(TAG, "Couldn't get json from server.");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(),
                                "Couldn't get json from server. Check LogCat for possible errors!",
                                Toast.LENGTH_LONG)
                                .show();
                    }
                });

            }

            return null;
        }

        /**
         * Updating parsed JSON data into ListView
         * */
        @Override
        protected void onPostExecute(Void result) {
            // Dismiss the progress dialog
            progressBar.setVisibility(android.view.View.GONE);
            //lv.setAdapter((ListAdapter) new CheckboxAdapter(getApplicationContext(),R.layout))

            super.onPostExecute(result);

            String[] myArr = ingredientList.toArray(new String[0]);
            //adapter = new CheckboxAdapter(getApplicationContext(),ingredientList);
            listAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.ingredient_item, R.id.ingredient,ingredientList);
            lv.setAdapter(listAdapter);

            setListeners();
        }
    }

    private void setListeners() {

        final ArrayList<String> tempList = ingredientList;

        Log.d("test", "children: " + lv.getCount());
        selected = new ArrayList<String>();

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("LISTENERS", "Setting listener...");
                //String myIngredient = tempList.get(position);
                sparseBooleanArray = lv.getCheckedItemPositions();

                String valueHolder = "";
                int i = 0;
                while(i < sparseBooleanArray.size()) {
                    if (sparseBooleanArray.valueAt(i)) {
                        String data = '"' + ingredientList.get(sparseBooleanArray.keyAt(i)) + '"';
                        if(!selected.contains(data)) {
                            selected.add(data);
                            valueHolder += data;
                        }
                        //lv.getChildAt(i).setBackgroundColor(0xff67daff);
                    }
                    else {
                        selected.remove('"' + ingredientList.get(sparseBooleanArray.keyAt(i)) + '"');
                        //lv.getChildAt(i).setBackgroundColor(0x000000);
                    }
                    i++;
                }
                valueHolder = valueHolder.replaceAll("(,)*$", "");
                Log.d("valueHolder", selected.toString());
                //Toast.makeText(MainActivity.this, "ListView selected values: " + valueHolder, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void giveRecipes(View view) {
        Intent intent = new Intent(this, ShowRecipes.class);

        ArrayList<String> iList = new ArrayList<>();

        /*
        SparseBooleanArray checked = lv.getCheckedItemPositions();

        Log.d("checked", "size" + checked.size());
        */

        String str = selected.toString();
        Log.d("sent...", str);
        intent.putExtra("iArray", str);
        startActivity(intent);
    }
}

