package fi.baariukot.baarikaappiapp;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;


public class ShowRecipes extends AppCompatActivity {

    private String iArrayString = "";
    private String TAG = MainActivity.class.getSimpleName();

    private ProgressBar progressBar;

    // URL to get recipe JSON
    private String baseUrl;
    private String drinkUrl;

    private ArrayList<String> drinks;
    private ListAdapter listAdapter;
    ListView lv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_recipes);

        baseUrl = getString(R.string.baseURL);
        drinkUrl = "http://" + baseUrl + ":3000/drinks/search?i=";

        drinks = new ArrayList<String>();

        Bundle extras = getIntent().getExtras();
        iArrayString = extras.getString("iArray");
        Log.d("Received...", iArrayString);

        lv = (ListView)findViewById(R.id.drink_list);
        new GetDrinks().execute();
    }

    /**
     * Async task class to get json by making HTTP call
     */
    private class GetDrinks extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar = new ProgressBar(getApplicationContext(), null, android.R.attr.progressBarStyleSmall);
            progressBar.setVisibility(android.view.View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... args) {
            HttpHandler httpHandler = new HttpHandler();
            String fullUrl = drinkUrl + iArrayString;
            Log.d("Full URL", fullUrl);

            String jsonString = httpHandler.makeServiceCall(fullUrl);

            Log.d(TAG, "Response from url: " + jsonString);

            if (jsonString != null) {
                try {
                    JSONArray returnedDrinks = new JSONArray(jsonString);
                    for (int i = 0; i < returnedDrinks.length(); i++) {
                        drinks.add(returnedDrinks.getJSONObject(i).getString("name"));
                    }
                }
                catch (final JSONException e) {
                    Log.e(TAG, "Json parsing error: " + e.getMessage());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),
                                    "Json parsing error: " + e.getMessage(),
                                    Toast.LENGTH_LONG)
                                    .show();
                        }
                    });
                }
            }

            else {
                Log.e(TAG, "Couldn't get json from server.");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(),
                                "Couldn't get json from server. Check LogCat for possible errors!",
                                Toast.LENGTH_LONG)
                                .show();
                    }
                });

            }

            return null;
        }

        /**
         * Updating parsed JSON data into ListView
         * */
        @Override
        protected void onPostExecute(Void result) {
            // Dismiss the progress dialog
            progressBar.setVisibility(android.view.View.GONE);
            //lv.setAdapter((ListAdapter) new CheckboxAdapter(getApplicationContext(),R.layout))

            super.onPostExecute(result);

            String[] myArr = drinks.toArray(new String[0]);
            //adapter = new CheckboxAdapter(getApplicationContext(),ingredientList);
            listAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.list_item, R.id.name,drinks);
            lv.setAdapter(listAdapter);

            setListeners();
        }
    }

    private void setListeners() {
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String drink = drinks.get(position);

                Intent intent = new Intent(ShowRecipes.this,ChosenRecipe.class);
                intent.putExtra("drink", drink);
                Log.d("Sending...", drink);
                startActivity(intent);
            }
        });
    }

    public void getRecipe(View view) {
        Intent intent = new Intent(this, ChosenRecipe.class);
        startActivity(intent);
    }
}
